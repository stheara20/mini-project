@extends('layouts.master')
@section('title','Edit Person')
@section('content')
        
<div class="row">
    <div class="col-md-12">
        <form role="form" method="POST" action="{{Route('update.person',$person->id)}}" enctype="multipart/form-data"> 
            @method("PUT")
            @csrf
            <div class="form-group">
                 
                <label>
                    @lang("lang.name")
                </label>
               <input type="text" class="form-control" name="name" value="{{$person->name}}"/>
               @error('name')
                <span style="color:red">@lang("lang.null")</span>
                @enderror
            </div>
            <div class="form-group">
                <label >
                    @lang("lang.email")
                </label>
                <input type="mail" class="form-control" name="email" value="{{$person->email}}" />
                @error('email')
                <span style="color:red">@lang("lang.null")</span>
                @enderror
            </div>
            <div class="form-group">
                 @lang("lang.phone")
                </label>
               <input type="text" class="form-control" name="phone_number" value="{{$person->phone_number}}"/>
               @error('phone_number')
               <span style="color:red">@lang("lang.null")</span>
               @enderror
            </div>
 
            <div class="form-group">
                    <label >
                            @lang("lang.profile")
                    </label>
                <img src="/storage{{$person->profile}}" alt="Avatar" class="rounded-circle" style="with:200px;height:200px;" > 
                
                <input type="file" class="form-control-file" name="profile" />
                
            </div>
            
            <button type="submit" class="btn btn-primary">
                @lang("lang.update")
            </button>
        </form>
    </div>
</div>
@endsection