@extends('layouts.master')
@section('title','Home')
@section('content')

<div class="panel panel-primary">
    <div class="panel-body" style="margin-top:10px;">
        <form class="form-inline md-form mr-auto mb-2" action="/search" method="POST">
            @csrf
                <table>
                    <tr>
                        <td><input class="form-control" type="text" placeholder="Search" aria-label="Search" name="txtsearch" value={{isset($text)?$text:''}}></td>
                        <td>
                                <select class="custom-select form-control "  name="filter">
                                        @foreach($filters as $item)
                                          <option value="{{$item}}"
                                              @if($item==$filter)
                                               selected="selected"
                                              @endif
                                          >{{$item}}
                                        </option>
                                        {{-- <option value="a">asdfasd</option> --}}
                                  @endforeach
                                   
                                </select> 
                        </td>
                        <td>
                                @lang("lang.fromdate") <input type="text" name="dFrom" class="startdate form-control" value={{isset($dateFrom)?$dateFrom:''}}>
                        </td>
                        <td>
                                @lang("lang.todate") <input type="text" name="dTo" class="enddate form-control" value={{isset($dateTo)?$dateTo:''}}>
                        </td>
                        <td>
                                <a href="{{Route("home")}}"><input type="submit" class="btn btn-success" value="@lang("lang.search")"></button></a>
                        </td>
                    </tr>
                </table>                      
        </form>
    </div>
  </div>



<table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang("lang.profile")</th>
                <th>@lang("lang.name")</th>
                <th>@lang("lang.email")</th>
                <th>@lang("lang.phone")</th>
                
                <th>@lang("lang.cdate")</th>
                <th>@lang("lang.udate")</th>
                <th>@lang("lang.action")</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($persons as $person)
            <tr class="table-success">
                <td>{{$person->id}}</td>
                <td>  <img src="storage/{{$person->profile}}" alt="Avatar" class="avatar">  </td>
                <td>{{$person->name}}</td>
                <td> {{$person->email}}</td>
                <td> {{$person->phone_number}}</td>  
                <td>{{$person->created_at}}</td>
                 <td>{{$person->updated_at}}</td>
                <td>
                        <a href="{{Route('view.person',$person->id)}}"><input type="button" class="btn btn-success" value="@lang("lang.view")"></button></a>
                        <a href="{{Route('edit.person',$person->id)}}"><input type="button" class="btn btn-primary" value="@lang("lang.edit")"></button></a>
                        <a href="{{Route('delete.person',$person->id)}}"><input type="button" class="btn btn-danger" value="@lang("lang.del")"></button></a>
                
                </td>
                
            </tr>
            @endforeach
            <tr> <td colspan="6">{{$persons->links()}}</td> </tr>   
        </tbody>
        
    </table>
    
</div>
    
@endsection