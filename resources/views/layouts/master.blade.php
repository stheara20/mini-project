<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <style>
        .avatar {
          vertical-align: middle;
          width: 50px;
          height: 50px;
          border-radius: 50%;
        }
        </style>
    <title>User-@yield('title')</title>
</head>
<body>
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-dark bg-dark static-top">
                             
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="navbar-toggler-icon"></span>
                            </button> <a class="navbar-brand" href="{{Route("home")}}">@lang("lang.brand")</a>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="navbar-nav">
                                    <li class="nav-item active">
                                         <a class="nav-link" href="{{Route('create.person')}}">@lang("lang.addUser") </span></a>
                                    </li>
                                    <li class="nav-item active">
                                        <a class="nav-link" href="{{Route('english')}}">English </span></a>
                                   </li>
                                   <li class="nav-item active">
                                    <a class="nav-link" href="{{Route('khmer')}}">Khmer </span></a>
                                   @if(!Auth::check())
                                   <li>
                                      <a href="/login"><input type="submit" class="btn btn-success" value="@lang("lang.login")"></button></a>
                                  </li>
                                  @else 
                                  <li>
                                      <a href="/logout"><input type="submit" class="btn btn-success" value="@lang("lang.logout")"></button></a>
                                  </li>
                                  @endif
                               </li>
                                    
                                </ul>
                                
                                
                            </div>
                            

                        </nav>
                        
                       
                </div>
            </div>
            @yield('content')
</body>
<script>
    $( function() {
      $( ".startdate" ).datepicker({
      
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true
      });
      $('.startdate').datepicker('setDate', 'today');
      $( ".enddate" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true
      });
      $('.enddate').datepicker('setDate', 'today');
      
          
    } );
    function getToday(){
      return $.datepicker.formatDate('yy/mm/dd', new Date());
    }
  
    
  </script>

</html>